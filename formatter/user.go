package formatter

import "belajar_git/entity"

type userFormatter struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Role     string `json:"roles"`
}

func FormatUser(user entity.User) userFormatter {
	users := userFormatter{}
	users.ID = user.ID
	users.Email = user.Email
	users.Password = user.Password
	users.Role = user.Role
	return users
}
